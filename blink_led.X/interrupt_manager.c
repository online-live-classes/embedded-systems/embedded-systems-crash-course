
#include <xc.h>
#include "interrupt_manager.h"
#include "system_setting.h"

/*
 * 
 */
void  INTERRUPT_Initialize (void)
{
    // Disable Interrupt Priority Vectors (16CXXX Compatibility Mode)
    RCONbits.IPEN = 0;
}


/*
 * 
 */
void __interrupt() INTERRUPT_InterruptManager (void)
{
    // interrupt handler
    if(INTCONbits.TMR0IE == 1 && INTCONbits.TMR0IF == 1)
    {
        TMR0_ISR();
    }
    else if(INTCONbits.PEIE == 1)
    {
        
    }      
    else
    {
        //Unhandled Interrupt
    }
}
