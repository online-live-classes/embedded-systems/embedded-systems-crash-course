/* 
 * File:   system_setting.c
 * Author: iqwindows
 *
 * Created on 2 May, 2020, 4:25 PM
 */

#include "system_setting.h"



/*
 * 
 */
void OSCILLATOR_Initialize(void)
{
    // IDLEN 0; IRCF 8MHz; OSTS 0; IOFS 0; SCS 00; 
    OSCCON = 0x70;

    // INTSRC disabled; TUN 0; 
    OSCTUNE = 0x00;
}

/*
 * 
 */
void SYSTEM_Initialize(void) {

    OSCILLATOR_Initialize();
    INTERRUPT_Initialize();
    TMR0_Initialize();
    PIN_MANAGER_Initialize();
}

