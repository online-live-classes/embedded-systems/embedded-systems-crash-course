/*
 * File:   app.c
 * Author: iqwindows
 *
 * Created on 2 May, 2020, 4:42 PM
 */


#include "app.h"
#include "system_setting.h"

bool app_tmr0_interrupt = false;

/*
 * 
 */
void app_tmr0_interrupt_handle() {
    app_tmr0_interrupt = true;
}

/*
 * 
 */
void APP_Initialize(void) {

    // Set the RD1 Pin as Digital Out and default signal as LOW
    IO_RD1_SetDigitalOutput();
    IO_RD1_SetLow();

    // Set the RB1 Pin as Digital Out and default signal as LOW
    IO_RB1_SetDigitalOutput();
    IO_RB1_SetLow();

    // Set the TMR0 Interrupt Handler for Application
    TMR0_SetInterruptHandler(app_tmr0_interrupt_handle);
}

/*
 * 
 */
void APP_Execution(void) {

    if (app_tmr0_interrupt == true) {

        IO_RD1_Toggle();
        IO_RB1_Toggle();

        app_tmr0_interrupt = false;
    }


}
