/* 
 * File:   system_setting.h
 * Author: iqwindows
 *
 * Created on 2 May, 2020, 4:26 PM
 */

#ifndef SYSTEM_SETTING_H
#define	SYSTEM_SETTING_H

#ifdef	__cplusplus
extern "C" {
#endif
    

#include <stdio.h>
#include <stdlib.h>
    
#include <xc.h>
#include "config_4550.h"
#include "interrupt_manager.h"
#include "tmr0.h"
#include "pin_manager.h"



/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Initializes the device to the default states configured
 * 
 * @Example
    system_init(void);
 */
void SYSTEM_Initialize(void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Initializes the oscillator to the default states configured.
 * @Example
    oscillator_init(void);
 */
void OSCILLATOR_Initialize(void);


#ifdef	__cplusplus
}
#endif

#endif	/* SYSTEM_SETTING_H */

