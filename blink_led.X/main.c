/* 
 * File:   main.c
 * Author: iqwindows
 *
 * Created on 2 May, 2020, 3:46 PM
 */

#include "system_setting.h"
#include "interrupt_manager.h"
#include "app.h"


/*
 * 
 */
void main() {

    // Initialize the device
    SYSTEM_Initialize();
    
    // If using interrupts in PIC18 High/Low Priority Mode you need to enable the Global High and Low Interrupts
    // If using interrupts in PIC Mid-Range Compatibility Mode you need to enable the Global and Peripheral Interrupts
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();

    // Initialize the application
    APP_Initialize();
    
    while (1) //main loop
    {
        // Execute application
        APP_Execution();
    }
}

