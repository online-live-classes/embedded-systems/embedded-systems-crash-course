/* 
 * File:   app.h
 * Author: iqwindows
 *
 * Created on 2 May, 2020, 4:43 PM
 */

#ifndef APP_H
#define	APP_H

#ifdef	__cplusplus
extern "C" {
#endif

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Initializes Application peripherals
 * @Example
    APP_Initialize();
 */
void APP_Initialize(void);


/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Execution of the Application
 * @Example
    APP_Execution();
 */
void APP_Execution(void);


#ifdef	__cplusplus
}
#endif

#endif	/* APP_H */

