### Prerequisites

* Basic knowledge of C Programming
* Basic knowledge of Micro Controllers

### Course Content

#### Day 1

* Indroduction to Embedded Systems and its Terminologies
* Usage of C/C++ in Embedded Systems
* Concept of Cross Compilation
* Difference between Programming on Host and Target

#### Day 2

* Basics of Micro Controllers
* The PIC18 Micro Controllers
* Understanding Datasheets
* Tools needed for programming
* Writing a Hello World program for PIC18

#### Day 3

* Hands on programming for PIC18
* Interface various peripherals
* Build, Flash and Run

#### Day 4

* Hands on programming for PIC18
* Interface various peripherals
* Build, Flash and Run
* Mini Project

#### Day 5

* Mini Project
* Industrial Use Cases of Embedded Systems
* Application of Embedded Systems in IoT